package de.szut.lf8_project.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GetEmployeeQualificationsDto {

    public int id;
    public String firstname;
    public String lastname;
    public List<GetQualificationDto> qualifications;


}
