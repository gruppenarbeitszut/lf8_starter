package de.szut.lf8_project.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class AddEmployeeDto {

    private int EmployeeId;
    private String ProjectRole;

    @JsonCreator
    AddEmployeeDto(@JsonProperty int EmployeeId, @JsonProperty String EmployeeRole) {
        this.EmployeeId = EmployeeId;
        this.ProjectRole = EmployeeRole;
    }
}
