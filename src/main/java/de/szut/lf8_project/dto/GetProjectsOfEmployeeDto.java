package de.szut.lf8_project.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class GetProjectsOfEmployeeDto {
    private int ProjectId;
    private String ProjectName;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Date startDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Date estimatedEndDate;

    private String ProjectRole;



}
