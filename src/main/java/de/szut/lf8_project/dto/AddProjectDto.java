package de.szut.lf8_project.dto;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
@Data
public class AddProjectDto {

    private int ProjectID;

    @NotBlank(message = "Error: ProjectName is mandatory")
    @Size(max = 50, message = "ProjectName must not exceed 50 characters")
    private String ProjectName;

    @NotNull
    private int customerId;

    @NotBlank(message = "Error: customerContact is mandatory")
    @Size(max = 50, message = "customerContact must not exceed 50 characters")
    private String customerContact;

    @NotBlank(message = "Error: comment is mandatory")
    @Size(max = 50, message = "comment must not exceed 50 characters")
    private String comment;

    @NotNull
    private Date startDate;

    private Date estimatedEndDate;

    @NotNull
    private Date actualEndDate;

    @ElementCollection
    private List<AddEmployeeDto> Employees;
}
