package de.szut.lf8_project.dto;
import de.szut.lf8_project.Entities.ProjectEmployeeEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import java.util.List;

@AllArgsConstructor
@Getter
@Setter
public class AddEmployeeToProjectDto {
    private int ProjectId;
    private String ProjectName;
    private List<ProjectEmployeeEntity> Employees;

}
