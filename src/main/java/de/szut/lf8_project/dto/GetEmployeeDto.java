package de.szut.lf8_project.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GetEmployeeDto {
    int id;
    String firstName;
    String lastName;
    String postcode;
    String city;
    String phone;
}
