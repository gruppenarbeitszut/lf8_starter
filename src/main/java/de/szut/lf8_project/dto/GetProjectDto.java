package de.szut.lf8_project.dto;


import java.sql.Date;
import java.util.List;

import de.szut.lf8_project.Entities.ProjectEmployeeEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class GetProjectDto {
    private long  id;
    private String name;
    private int customerId;
    private String customerContact;
    private String comment;
    private Date startDate;
    private Date estimatedEndDate;
    private Date actualEndDate;
    private List<ProjectEmployeeEntity> Employees;

}
