package de.szut.lf8_project.dto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@Getter
@Setter
public class GetAllProjectsOfEmployeeDto {

    private int EmployeeId;
    private List<GetProjectsOfEmployeeDto> ProjectList;


}
