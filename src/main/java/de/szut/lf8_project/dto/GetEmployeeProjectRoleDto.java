package de.szut.lf8_project.dto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class GetEmployeeProjectRoleDto {

    private int ProjectId;
    private int EmployeeId;
    private String EmployeeRole;

}
