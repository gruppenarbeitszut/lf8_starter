package de.szut.lf8_project.Repositories;
import org.springframework.data.jpa.repository.JpaRepository;
import de.szut.lf8_project.Entities.ProjectEntity;
import de.szut.lf8_project.model.Project;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ProjectRepository extends JpaRepository<ProjectEntity, Long> {
    public Project findByDesignation(String name);
    Optional<ProjectEntity> findById(long id);
}