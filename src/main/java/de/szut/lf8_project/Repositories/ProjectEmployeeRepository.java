package de.szut.lf8_project.Repositories;

import de.szut.lf8_project.Entities.ProjectEmployeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ProjectEmployeeRepository extends JpaRepository<ProjectEmployeeEntity, Long> {
    @Query("SELECT e FROM ProjectEmployeeEntity e WHERE e.EmployeeId = :employeeId and ((e.Project.actualEndDate >= :startDate and e.Project.startDate <= :actualEndDate) or (e.Project.startDate >= :startDate and e.Project.actualEndDate <= :actualEndDate))")
    List<ProjectEmployeeEntity> GetProjectByID(int id);
    Optional<ProjectEmployeeEntity> GetProjectByProjectIDAndEmployeeID(long projectId, long employeeId);
    List<ProjectEmployeeEntity> GetByEmployeeID(int id);
    List<ProjectEmployeeEntity> GetByEmployeeIdAndStartDateAndEndDate(long employeeId, Date startDate, Date endDate);
}