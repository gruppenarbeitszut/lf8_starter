package de.szut.lf8_project.Entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "Projects")

public class ProjectEntity {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private int id;
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private String name;
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private int customerId;
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private String CustomerContact;
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private String comment;
        @JsonInclude(JsonInclude.Include.NON_NULL)
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
        private Date startDate;
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
        private Date estimatedEndDate;
        @JsonInclude(JsonInclude.Include.NON_NULL)
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
        private Date actualEndDate;
        @JsonInclude(JsonInclude.Include.NON_NULL)
        @OneToMany(mappedBy = "Project",  fetch = FetchType.EAGER,cascade = CascadeType.ALL)
        private List<ProjectEmployeeEntity> Employees;

public ProjectEntity(String name, int customerId, String CustomerContact, String comment, Date startDate, Date estimatedEndDate, Date actualEndDate, List<ProjectEmployeeEntity> Employees){
      this.name = name;
      this.customerId = customerId;
      this.CustomerContact = CustomerContact;
      this.comment = comment;
      this.startDate = startDate;
      this.estimatedEndDate = estimatedEndDate;
      this.actualEndDate = actualEndDate;
      this.Employees = Employees;
}


}
