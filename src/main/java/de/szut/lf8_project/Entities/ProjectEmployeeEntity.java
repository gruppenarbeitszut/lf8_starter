package de.szut.lf8_project.Entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.persistence.*;
import java.sql.Date;
import java.util.Optional;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "ProjectEmployees")

public class ProjectEmployeeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "project_id")
    private ProjectEntity Project;
    private int ProjectID;
    private int EmployeeId;
    private String EmployeeRole;


    public ProjectEmployeeEntity(Optional<ProjectEntity> projectEntity, int EmployeeId, String EmployeeRole){
        this.Project = projectEntity.get();
        this.EmployeeId = EmployeeId;
        this.EmployeeRole = EmployeeRole;
    }


}
