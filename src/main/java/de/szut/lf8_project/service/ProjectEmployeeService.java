package de.szut.lf8_project.service;

import de.szut.lf8_project.Entities.*;
import de.szut.lf8_project.exceptionHandling.*;
import de.szut.lf8_project.Repositories.*;
import de.szut.lf8_project.dto.*;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ProjectEmployeeService {
    private RestTemplate Rest = new RestTemplate();
    @Autowired
    private ProjectEmployeeRepository PERepository;
    @Autowired
    private ProjectRepository projectsRepo;

    ProjectEmployeeService() {

    }

    public List<ProjectEmployeeEntity> getAllEmployeesByProject(int id) {
        return this.PERepository.GetProjectByID(id);
    }

    public Optional<ProjectEmployeeEntity> findEmployeeProjectById (long projectId, long employeeId) {
        return this.PERepository.GetProjectByProjectIDAndEmployeeID(projectId, employeeId);
    }

    @Transactional
    public void deleteEmployeeFromProject (ProjectEmployeeEntity employee, ProjectEntity project) {
        employee.setProject(null);
        project.getEmployees().remove(employee);
        this.PERepository.delete(employee);
    }
    public void checkIfEmployeeExists(long id, String token) {
        HttpHeaders httpHeaders = new HttpHeaders();
        String tokenSplit = token.split(" ") [1];
        httpHeaders.setBearerAuth(tokenSplit);
        HttpEntity<String> entity = new HttpEntity<>("body", httpHeaders);
        String queryUrl = String.format("https://employee.szut.dev/employees/%s/", id);
        try{ this.Rest.exchange(queryUrl, HttpMethod.GET, entity, GetEmployeeQualificationsDto.class);
        }
        catch (HttpClientErrorException e) {
            switch (e.getRawStatusCode()) {
                case 404:
                    throw new ResourceNotFoundException(String.format("No Employee found for ID: %s ", id));
                case 501:
                    throw new NotAuthorizedException("Not authorized");
                default:
                    throw new ServerErrorException("Eroor");
            }
        }
    }
    public ResponseEntity<GetEmployeeQualificationsDto> getEmployeeQualification(long id, String token) {
        HttpHeaders httpHeaders = new HttpHeaders();
        String tokenSplit = token.split(" ") [1];
        httpHeaders.setBearerAuth(tokenSplit);
        HttpEntity<String> entity = new HttpEntity<>("body", httpHeaders);
        String queryUrl = String.format("https://employee.szut.dev/employees/%s/qualifications", id);
        return this.Rest.exchange(queryUrl, HttpMethod.GET, entity, GetEmployeeQualificationsDto.class);
    }
    public void checkifEmployeeIsAlreadyIncludedInAProject(GetEmployeeProjectRoleDto employeeProjectRoleDto, ProjectEntity projectsEntity) {
        if(!this.PERepository.GetByEmployeeIdAndStartDateAndEndDate(employeeProjectRoleDto.getEmployeeId(), projectsEntity.getStartDate(), projectsEntity.getActualEndDate()).isEmpty()) {
            throw new ResourceNotFoundException(String.format("The Employee %s is already included in other Projects over that time period", employeeProjectRoleDto.getEmployeeId()));
        }
    }
    public List<ProjectEmployeeEntity> getAllProjectsOfEmployee(int id){
        return this.PERepository.GetByEmployeeID(id);
    }
    public void checkEmployeeQualification(GetEmployeeProjectRoleDto employeeProjectRoleDto, String token) {
        var responseEntity = getEmployeeQualification(employeeProjectRoleDto.getEmployeeId(), token);
        for (GetQualificationDto skill : responseEntity.getBody().getQualifications()) {
            if (skill.DescriptionOfQualification.equals(employeeProjectRoleDto.getEmployeeRole())) {
                return;
            }
        }
        throw new ResourceNotFoundException(String.format("The Employee %s doesn't have the necessary qualification for the project role", employeeProjectRoleDto.getEmployeeId()));
    }

    public Optional<ProjectEntity> addEmployees(GetEmployeeProjectRoleDto employeeProjectRoleDto) {
        var projectEntity = this.projectsRepo.findById(employeeProjectRoleDto.getProjectId());
        var newEmployeeEntity = new ProjectEmployeeEntity(projectEntity, employeeProjectRoleDto.getEmployeeId(), employeeProjectRoleDto.getEmployeeRole());
        this.PERepository.save(newEmployeeEntity);
        projectEntity.get().getEmployees().add(newEmployeeEntity);
        return projectEntity;
    }
    public void checkIfEmployeeAlreadyInProject(GetEmployeeProjectRoleDto employeeProjectRoleDto) {
        var project = this.projectsRepo.findById(employeeProjectRoleDto.getProjectId());
        var employeeMap = project.get().getEmployees().stream().collect(Collectors.toMap(ProjectEmployeeEntity::getEmployeeId, Function.identity()));
        if(employeeMap.get(employeeProjectRoleDto.getEmployeeId()) != null) {
            throw new ResourceNotFoundException(String.format(" The Employee %s is included in project %s", employeeProjectRoleDto.getEmployeeId(), employeeProjectRoleDto.getProjectId()));
        }
    }





}


