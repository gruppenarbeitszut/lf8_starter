package de.szut.lf8_project.service;

import de.szut.lf8_project.Entities.ProjectEntity;
import de.szut.lf8_project.Repositories.ProjectRepository;
import de.szut.lf8_project.Entities.ProjectEmployeeEntity;
import de.szut.lf8_project.Mapper.ProjectEmployeeMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ProjectService {

    @Autowired
    private ProjectRepository PRepository;
    @Autowired
    private ProjectEmployeeService employeeProjectService;
    @Autowired
    private ProjectEmployeeMapper employeeProjectMapper;
    private RestTemplate restTemplate = new RestTemplate();

    public ProjectService() {

    }

    public void update(ProjectEntity newProjectEntity, int id, String Token){
        var oldProjectEntity = this.PRepository.findById(id).get();
        oldProjectEntity.setName(newProjectEntity.getName());
        oldProjectEntity.setCustomerId(newProjectEntity.getCustomerId());
        oldProjectEntity.setCustomerContact(newProjectEntity.getCustomerContact());
        oldProjectEntity.setComment(newProjectEntity.getComment());
        oldProjectEntity.setStartDate(newProjectEntity.getStartDate());
        oldProjectEntity.setEstimatedEndDate(newProjectEntity.getEstimatedEndDate());
        oldProjectEntity.setActualEndDate(newProjectEntity.getActualEndDate());
        oldProjectEntity.setEmployees(updateEmployee(oldProjectEntity.getEmployees(), newProjectEntity.getEmployees(), newProjectEntity, Token));
        oldProjectEntity.getEmployees().forEach( e-> {
            this.employeeProjectService.checkifEmployeeIsAlreadyIncludedInAProject(this.employeeProjectMapper.MapPEEntityAndGetEmployeeProjectRoleDto(e), oldProjectEntity);
            this.employeeProjectService.checkEmployeeQualification(this.employeeProjectMapper.MapPEEntityAndGetEmployeeProjectRoleDto(e), Token);
        });
        this.PRepository.save(oldProjectEntity);
    }

    public ProjectEntity Add(ProjectEntity entity) {
        return this.PRepository.save(entity);
    }

    public List<ProjectEntity> GetAll() {
        return this.PRepository.findAll();
    }


    private List<ProjectEmployeeEntity> updateEmployee(List<ProjectEmployeeEntity> oldEmployeeList, List<ProjectEmployeeEntity> newEmployeeList, ProjectEntity projectsEntity, String token){
        var employeeMap = oldEmployeeList.stream().collect(Collectors.toMap(ProjectEmployeeEntity::getEmployeeId, Function.identity()));
        newEmployeeList.forEach(e -> {
            employeeMap.get(e.getEmployeeId()).setEmployeeRole(e.getEmployeeRole());});
        return employeeMap.values().stream().collect(Collectors.toList());
    }

    public ProjectEntity GetById(int id) {
        Optional<ProjectEntity> optQualification = this.PRepository.findById(id);

        if (!optQualification.isPresent()) {
            return null;
        }

        return optQualification.get();
    }

    public void delete(ProjectEntity entity) {
        this.PRepository.delete(entity);
    }



}