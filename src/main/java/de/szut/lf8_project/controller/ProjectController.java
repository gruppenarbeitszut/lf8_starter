package de.szut.lf8_project.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import de.szut.lf8_project.Entities.*;
import de.szut.lf8_project.Mapper.ProjectEmployeeMapper;
import de.szut.lf8_project.Mapper.ProjectMapper;
import de.szut.lf8_project.exceptionHandling.*;
import de.szut.lf8_project.dto.*;
import de.szut.lf8_project.service.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;


@RequestMapping(value = "project")
@RestController
public class ProjectController {
   //@Autowired Bug
    private ProjectService service;
    @Autowired
    private ProjectEmployeeService ProjectEmployeeService;
    @Autowired
    private ProjectMapper projectsMapper;
    @Autowired
    private ProjectEmployeeMapper employeeProjectMapper;

    public ProjectController() {}

    @Operation(summary = "Add a project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Project was added successfully",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GetProjectDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid JSON",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not authorized",
                    content = @Content)})
    @PostMapping
    public ResponseEntity<GetProjectDto> create(@RequestHeader("Authorization") String token, @RequestBody @Valid AddProjectDto addProjectDto){
        ProjectEntity projectsEntity = this.projectsMapper.mapCreateDtoToEntity(addProjectDto);
        for (ProjectEmployeeEntity employee:projectsEntity.getEmployees()) {
            employee.setProject(projectsEntity);
            this.ProjectEmployeeService.checkIfEmployeeExists(employee.getEmployeeId(), token);this.ProjectEmployeeService.checkEmployeeQualification(this.employeeProjectMapper.MapPEEntityAndGetEmployeeProjectRoleDto(employee), token);
            this.ProjectEmployeeService.checkifEmployeeIsAlreadyIncludedInAProject(this.employeeProjectMapper.MapPEEntityAndGetEmployeeProjectRoleDto(employee), projectsEntity);
        }
        projectsEntity = this.service.Add(projectsEntity);
        var res =  this.projectsMapper.mapToGetDto(projectsEntity);
        return new ResponseEntity<>(res, HttpStatus.CREATED);
    }
//GetAll
    @Operation( summary = "Get all Projects")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Project list",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GetProjectDto.class))}),
            @ApiResponse(responseCode = "401", description = "Not authorized",
                    content = @Content)})
    @GetMapping
    public ResponseEntity<List<GetProjectDto>> findAll() {
        var res =  this.service.GetAll().stream().map(e -> this.projectsMapper.mapToGetDto(e)).collect(Collectors.toList());
        return new ResponseEntity<>(res, HttpStatus.OK);
    }
    //GetByID
    @Operation(summary = "Get project by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "find successful"),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "resource not found",
                    content = @Content)})
    @GetMapping("/projects/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<ProjectEntity> getProjectById( @PathVariable int id) {
        var entity = this.service.GetById(id);

        if (entity == null) {
            throw new ResourceNotFoundException("ProjectEntity not found on id = " + id);
        } else {
            return new ResponseEntity<>(entity, HttpStatus.OK);
        }
    }
//UpdateByID
    @Operation(summary = "Update Project by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Updated data successfully"),
            @ApiResponse(responseCode = "401", description = "Not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "404 Error not found",
                    content = @Content)})
    @PutMapping("/projects/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<Void> updateProjectById( @RequestBody ProjectEntity xProject, @PathVariable int id, @RequestHeader("Authorization") String token) {
        var entity = this.service.GetById(id);
        if (entity == null) {throw new ResourceNotFoundException("No ProjectEntity found for id = " + id);
        } else {this.service.update(xProject, id, token);return new ResponseEntity<>(HttpStatus.ACCEPTED);}
    }
//DeleteByID
    @Operation(summary = "Deletes a project by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "delete successful"),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "resource not found",
                    content = @Content)})
    @DeleteMapping
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> deleteById(@RequestParam int id) {
        var entity = this.service.GetById(id);
        if (entity == null) {throw new ResourceNotFoundException("ProjectEntity not found on id = " + id);
        } else {this.service.delete(entity);return new ResponseEntity<>(HttpStatus.NO_CONTENT);}
    }




}
