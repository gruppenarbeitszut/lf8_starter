package de.szut.lf8_project.controller;
import de.szut.lf8_project.Mapper.ProjectEmployeeMapper;
import de.szut.lf8_project.exceptionHandling.*;
import de.szut.lf8_project.dto.*;
import de.szut.lf8_project.service.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "Employee-project")

public class ProjectEmployeeController {

    @Autowired
    private ProjectEmployeeService ProjectEmployeeService;
    //@Autowired bug
    private ProjectService projectsService;
    @Autowired
    private ProjectEmployeeMapper ProjectEmployeeMapper;

    @Operation(summary = "Add a new Employee to an existing project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "add employee accepted", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = AddEmployeeToProjectDto.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted", content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized", content = @Content)})
    @PostMapping()
    public ResponseEntity<AddEmployeeToProjectDto> addEmployeeToProject(@RequestHeader("Authorization")  String token, @RequestBody @Valid GetEmployeeProjectRoleDto employeeProjectRoleDto){
        var projectEntity = this.projectsService.GetById(employeeProjectRoleDto.getProjectId());
        if(projectEntity == null) {
            throw new ResourceNotFoundException("No projects found!");
        }
        this.ProjectEmployeeService.checkIfEmployeeExists(employeeProjectRoleDto.getEmployeeId(), token);
        this.ProjectEmployeeService.checkIfEmployeeAlreadyInProject(employeeProjectRoleDto);
        this.ProjectEmployeeService.checkEmployeeQualification(employeeProjectRoleDto, token);
        this.ProjectEmployeeService.checkifEmployeeIsAlreadyIncludedInAProject(employeeProjectRoleDto, projectEntity);
        var res = this.ProjectEmployeeMapper.MapPEntityAndAddEmployeeToProjectDto(this.ProjectEmployeeService.addEmployees(employeeProjectRoleDto));
        return new ResponseEntity<>(res, HttpStatus.ACCEPTED);
    }

    @Operation(summary = "get all employees for a project by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "find successful"),
            @ApiResponse(responseCode = "401", description = "not authorized", content = @Content),
            @ApiResponse(responseCode = "404", description = "resource not found", content = @Content)})
    @GetMapping("/{id}")
    public ResponseEntity<List<GetEmployeeProjectRoleDto>> getProjectEmployeesById(@PathVariable int id) {
        var a = this.ProjectEmployeeService.getAllEmployeesByProject(id)
                .stream()
                .map(e -> this.ProjectEmployeeMapper.MapPEEntityAndGetEmployeeProjectRoleDto(e))
                .collect(Collectors.toList());
        if(!a.isEmpty()){
            return new ResponseEntity<>(a, HttpStatus.OK);
        } else {
            throw new ResourceNotFoundException("Project id not found!");
        }
    }

    @Operation(summary = "get all projects for an employee by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "find successful", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = GetProjectsOfEmployeeDto.class))}),
            @ApiResponse(responseCode = "401", description = "not authorized", content = @Content),
            @ApiResponse(responseCode = "404", description = "resource not found", content = @Content)})
    @GetMapping("/employee/{id}")
    public ResponseEntity<GetAllProjectsOfEmployeeDto> getProjectsOfEmployee(@RequestHeader("Authorization")  String token, @PathVariable int id) {
        this.ProjectEmployeeService.checkIfEmployeeExists(id, token);
        var a = new GetAllProjectsOfEmployeeDto(id, this.ProjectEmployeeService.getAllProjectsOfEmployee(id)
                .stream()
                .map(e -> this.ProjectEmployeeMapper.MapPEEntityAndGetProjectsOfEmployeeDto(e))
                .collect(Collectors.toList())
        );
        if(a.getProjectList() != null){
            return new ResponseEntity<>(a, HttpStatus.OK);
        }
        else {
            throw new ResourceNotFoundException("No projects found!");
        }
    }

    @Operation(summary = "Delete an Employee from an existing project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "deleted employee", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", description = "resource not found", content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized", content = @Content)})
    @DeleteMapping
    public ResponseEntity<Void> deleteEmployeeFromProject(@RequestParam int projectId, @RequestParam long employeeId){
        var project = this.projectsService.GetById(projectId);
        if(project == null) {
            throw new ResourceNotFoundException("Project id not found!");
        }
        var entity = this.ProjectEmployeeService.findEmployeeProjectById(projectId, employeeId);
        if (!entity.isPresent()) {
            throw new ResourceNotFoundException("Employee not found in project:" + projectId);
        } else {
            this.ProjectEmployeeService.deleteEmployeeFromProject(entity.get(), project);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
