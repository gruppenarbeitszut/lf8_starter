package de.szut.lf8_project.model;

import de.szut.lf8_project.dto.GetQualificationDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@Entity
public class Employee {

    @Id
    private Long id;

    private String lastName;

    private String firstName;

    private String street;

    private String postcode;

    private String city;

    private String phone;



}
