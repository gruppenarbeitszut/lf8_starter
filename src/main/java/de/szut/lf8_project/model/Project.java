package de.szut.lf8_project.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.util.Pair;

import javax.persistence.*;
import java.sql.Date;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "project_generator")
    @SequenceGenerator(name = "project_generator", sequenceName = "project_id_seq", allocationSize = 1)
    @Column(name = "ProjectId", nullable = false)
    private Long id;

    @Column(name = "ProjectName", nullable = false)
    private String name;

    @Column(name = "CustomerID", nullable = false)
    private int customerId;

    @Column(name = "Cusomter_Contact", nullable = false)
    private String customerContact;

    @Column(name = "Comment", nullable = true)
    private String comment;

    @Column(name = "Begin_Date", nullable = false)
    private Date startDate;

    @Column(name = "Estimated_End_Date", nullable = false)
    private Date estimatedEndDate;

    @Column(name = "End_Date", nullable = true)
    private Date actualEndDate;


}
