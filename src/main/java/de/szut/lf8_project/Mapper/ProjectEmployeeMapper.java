package de.szut.lf8_project.Mapper;
import de.szut.lf8_project.dto.*;
import de.szut.lf8_project.Entities.*;
import org.springframework.stereotype.Service;
import java.util.Optional;
@Service
public class ProjectEmployeeMapper {


    public GetEmployeeProjectRoleDto mapEntityToEmployeeGetDto(ProjectEmployeeEntity entity) {
        return new GetEmployeeProjectRoleDto(entity.getProject().getId(), entity.getEmployeeId(), entity.getEmployeeRole());
    }

    public AddEmployeeToProjectDto MapPEntityAndAddEmployeeToProjectDto(Optional<ProjectEntity> PEntity) {
        return new AddEmployeeToProjectDto(PEntity.get().getId(), PEntity.get().getName(), PEntity.get().getEmployees());
    }

    public GetEmployeeProjectRoleDto MapPEEntityAndGetEmployeeProjectRoleDto(ProjectEmployeeEntity PEEntity) {
        return new GetEmployeeProjectRoleDto(PEEntity.getProject().getId(), PEEntity.getEmployeeId(), PEEntity.getEmployeeRole());
    }

    public GetProjectsOfEmployeeDto MapPEEntityAndGetProjectsOfEmployeeDto(ProjectEmployeeEntity PEEntity){
        return new GetProjectsOfEmployeeDto(PEEntity.getProject().getId(), PEEntity.getProject().getName(), PEEntity.getProject().getStartDate(),PEEntity.getProject().getEstimatedEndDate(), PEEntity.getEmployeeRole());
    }


}
