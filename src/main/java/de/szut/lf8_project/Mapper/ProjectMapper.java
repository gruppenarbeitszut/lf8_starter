package de.szut.lf8_project.Mapper;

import de.szut.lf8_project.Entities.ProjectEmployeeEntity;
import org.springframework.stereotype.Service;
import de.szut.lf8_project.dto.*;
import de.szut.lf8_project.Entities.*;
import java.util.Date;
import java.util.stream.Collectors;

@Service
public class ProjectMapper {
    public GetProjectDto mapToGetDto(ProjectEntity PEntity) {
        return new GetProjectDto(PEntity.getId(), PEntity.getName(), PEntity.getCustomerId(), PEntity.getCustomerContact(),PEntity.getComment(),PEntity.getStartDate(),PEntity.getEstimatedEndDate(),PEntity.getActualEndDate(), PEntity.getEmployees());
    }
    public ProjectEmployeeEntity mapCreateEmployeeDtoToEntity(AddEmployeeDto dto) {
        var PEEntity = new ProjectEmployeeEntity();
        PEEntity.setEmployeeId(dto.getEmployeeId());
        PEEntity.setEmployeeRole(dto.getProjectRole());
        return PEEntity;
    }
    public ProjectEntity mapCreateDtoToEntity(AddProjectDto AddProject) {
        var PEntity = new ProjectEntity();
        PEntity.setName(AddProject.getProjectName());
        PEntity.setCustomerId(AddProject.getCustomerId());
        PEntity.setCustomerContact(AddProject.getCustomerContact());
        PEntity.setComment(AddProject.getComment());
        PEntity.setStartDate(AddProject.getStartDate());
        PEntity.setEstimatedEndDate(AddProject.getEstimatedEndDate());
        PEntity.setActualEndDate(AddProject.getActualEndDate());
        PEntity.setEmployees(AddProject.getEmployees().stream().map(Employee -> mapCreateEmployeeDtoToEntity(Employee)).collect(Collectors.toList()));

        return PEntity;
    }


}
