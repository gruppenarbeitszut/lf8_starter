CREATE TABLE IF NOT EXISTS Project (
                                       ProjectId serial PRIMARY KEY,
                                       ProjectName VARCHAR ( 50 ) NOT NULL,
                                       CustomerID INT NOT NULL,
                                       Cusomter_Contact VARCHAR ( 50 ) UNIQUE NOT NULL,
                                       Comment VARCHAR ( 50 ) ,
                                       Begin_Date TIMESTAMP NOT NULL ,
                                       Estimated_End_Date TIMESTAMP NOT NULL ,
                                       End_Date TIMESTAMP
);
